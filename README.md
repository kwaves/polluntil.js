# pollUntil.js #

_NOTE: This plugin may not be actively maintained or supported. **Use at your own risk!**_

***
## Dependencies ##

- jQuery (tested with 1.9.1)

## Defaults ##
```js
$.pollUntil({
	input: null, // Input parameter for test, e.g. test(input). Can be anything�jQuery element, string, JSON, etc.
	test: null, // Test function. Must return truthy value 
	interval: 500, // Frequency (in miliseconds) with which test function is run
	maxTries: 50, // Execute error callback if test has not returned true after this # of tries
	error: function(error) { // Callback function after test returns false or poll runs out of tries
		console.error(error);
	},
	success: null // Callback function after test returns true
});
```

## Examples ##

### Fire WHEN window loaded, or IF window already loaded ###
```js
var windowLoaded = false;

$(window).on('load', function(){
	windowLoaded = true;
});

$.pollUntil({
	test: function(){
		return windowLoaded;
	},
	interval: 500, // ms
	maxTries: 50,
	error: function(error) {
		
		console.error('windowLoaded error:', error);
		
	},
	success: function(results) {

		console.log('windowLoaded!', results);
		
	}
});
```

### Fire when element is detached from DOM ###
```js
var $blah = $('.blah');

function existsInDOM($el) {
	
	if ( jQuery.contains(document, $el[0]) ) {
    	return true;
	} else {
		return false;
	}

}

$.pollUntil({
	input: $blah,
	test: existsInDOM,
	interval: 500, // ms
	maxTries: 50,
	error: function(error) {
		
		console.error(error);
		
	},
	success: function(results) {
		
		console.log('pollUntil success! Results:', results);
		
	}
});
```