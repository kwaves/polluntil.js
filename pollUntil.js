// ==================================================================================
//
//	=pollUntil v1.1
//	https://bitbucket.org/kwaves/polluntil.js/
//
//	Keeps polling until a truthy condition is met, or a given # of tries is reached
//	(For when you're too lazy to connect to an API callback, or one isn't available)
//
// ==================================================================================

(function ($) {

	$.pollUntil = function(params) {

		// console.log('pollUntil running!'); // FOR DEBUGGING		
    
    var params = params || {}; // Make sure params aren't undefined
    
    var opts = $.extend({
    	input: null, // Can be anything–element, JSON, etc.
    	test: null,
			interval: 500,
			maxTries: 50,	    
    	error: function(error){
	    	
	    	console.error(error);
	    	
    	},
    	success: null
    }, params); // Overwrite defaults with input params
				
		if ( typeof opts.test === 'function' && typeof opts.success === 'function' ) {
			
			var testResult = opts.test(opts.input);
						
			// If successful...
			if ( testResult ) {
	
				// Pass successful result to callback
				opts.success(testResult); // (could be true, a string, an object, an element, a non-zero number, etc.)
			
			// If unsuccessful but has maxTries left
			} else if ( typeof opts.maxTries != 'undefined' && opts.maxTries > 0 ) {
							
				setTimeout(function(){
					
					opts.maxTries = opts.maxTries-1;
						
					// Pass same input back minus 1 try
					$.pollUntil(opts);
					
				}, opts.interval);
				
			} else {
				
				opts.error('Ran out of tries!'); // (could be true, a string, an object, an element, a non-zero number, etc.)
				
			}
		
		} else {
			
			console.error('pollUntil: either test or success input is not a function.');
			
		}
		
	}

}(jQuery));